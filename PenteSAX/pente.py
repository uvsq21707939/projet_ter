
import numpy as np
import csv
import math 
from scipy import *
from scipy import optimize
import matplotlib.pyplot as plt
import time
from scipy import stats
from trie import TrieNode,add,shox,find_word
def SeparateWORD(data):
    words=[]
    
    i=0
    numeroSeq=0
    while i< len(data):
        tmpALPHA=''
        word=[]
        debut=True
        while (ord(data[i][0])>=ord('a') and ord(data[i][0])<=ord('i') ):
            if  debut:
             tmpALPHA=data[i][0]
             debut=False
            else:
             tmpALPHA=tmpALPHA+data[i][0]
            
            if(i< len(data)-1):
                 i+=1
            else:
                 word.append(tmpALPHA)
                 words.append(word)
                 return words      
        
        if not debut:
          word.append(tmpALPHA)
          words.append(word)
        debut=True
        tmpALPHA=''
        word=[]

            
        while(ord(data[i][0])>=ord('A') and ord(data[i][0])<=ord('I')  ):
                if  debut:
                  tmpALPHA=data[i][0]
                  debut=False
                else:
                  tmpALPHA=tmpALPHA+data[i][0]
                if(i< len(data)-1): 
                 i+=1
                else:
                 word.append(tmpALPHA)
                 words.append(word)
                 return words
        if not debut:
                word.append(tmpALPHA)
                words.append(word)
                
        
    return words
        
def Match(data,resultat,a,b,valD,valF,ymatch,xinit):

  if b<len(data):
    
    for x in range(xinit,ymatch):
      for y in range(0,len(data[b])):
          
            if (data[a][x][2])+1==(data[b][y][2]):
                if (a==0) and (b==len(data)-1):
                 Match(data,resultat,a+1,b+1,data[a][x][0],data[b][y][1],y+1,y)
                elif (a==0) :
                 Match(data,resultat,a+1,b+1,data[a][x][0],valF,y+1,y)
                elif b==len(data)-1:
                 Match(data,resultat,a+1,b+1,valD,data[b][y][1],y+1,y)
                else:
                 Match(data,resultat,a+1,b+1,valD,valF,y+1,y)
            

  else:
    t=[]
    t.append(valD)
    t.append(valF)
    resultat.append(t)
    return resultat

def alphabitize(data):
    tab=[]
    i=0
    alphaASC=['A','B','C','D','E','F','G','H','I']
    alphaDSC=['a','b','c','d','e','f','g','h','i']
    while i<len(data):
        if data[i][0]>=0:
         resu=data[i][0]/10
         tmp=[]
         tmp.append(alphaASC[int(resu)])
         tmp.append(data[i][1])
         tmp.append(data[i][2])
         tab.append(tmp)
                 
        else :
         resu=-data[i][0]/10
         tmp=[]
         tmp.append(alphaDSC[int(resu)])
         tmp.append(data[i][1])
         tmp.append(data[i][2])
         tab.append(tmp)
        i+=1
    return tab

def generateWord(data):
    words=[]
    
    i=0
    numeroSeq=0
    while i< len(data):
        tmpALPHA=''
        tmpDATED=''
        word=[]
        debut=True
        while (ord(data[i][0])>=ord('a') and ord(data[i][0])<=ord('i') ):
            if  debut:
             tmpALPHA=data[i][0]
             tmpDATED=data[i][1]
             debut=False
            else:
             tmpALPHA=tmpALPHA+data[i][0]
            
            if(i< len(data)-1):
                 i+=1
            else:
                 word.append(tmpALPHA)
                 word.append(tmpDATED)
                 word.append(data[i][2])
                 word.append(numeroSeq)
                 words.append(word)
                 return words      
        
        if not debut:
          word.append(tmpALPHA)
          word.append(tmpDATED)
          word.append(data[i-1][2])
          word.append(numeroSeq)
          words.append(word)
          numeroSeq+=1
        debut=True
        tmpALPHA=''
        tmpDATED=''
        word=[]
        
        	
            
        while(ord(data[i][0])>=ord('A') and ord(data[i][0])<=ord('I')  ):
                if  debut:
                  tmpALPHA=data[i][0]
                  tmpDATED=data[i][1]
                  debut=False
                else:
                  tmpALPHA=tmpALPHA+data[i][0]
                if(i< len(data)-1): 
                 i+=1
                else:
                 word.append(tmpALPHA)
                 word.append(tmpDATED)
                 word.append(data[i][2])
                 word.append(numeroSeq)
                 words.append(word)
                 return words
        if not debut:
                word.append(tmpALPHA)
                word.append(tmpDATED)
                word.append(data[i-1][2])
                word.append(numeroSeq)
                words.append(word)
                numeroSeq+=1
        
    return words
def main(): 
   
    td=time.time()
    date=[]
    Y=[]
    X=arange(2400)
              
    
    fname = "test.csv"
    file = open(fname, "rt")
    try:
     reader=csv.reader(file)
     for row in reader:
          tmp=[]
        
          date.append(row[0])  
          Y.append(float(''.join(row[1])))
         
    finally:
     file.close()
    
    resultat=[]
    paa=2
    i=0
    angle=[]
    
    while i<2400 :
        tmp=[]
        x=X[i:i+paa]
        y=Y[i:i+paa]
        a, b,rvalue,pvalue,err = stats.linregress(x, y)
        alpha=math.degrees(math.atan(a))
        tmp.append(alpha)
        tmp.append(date[i])
        tmp.append(date[i+1])
        angle.append(tmp)
        i+=paa
    tab=alphabitize(angle)
    data=generateWord(tab)
   
    print(time.time()-td)
    print ("******** résultat de la symbolisation Pente:********")
    print(tab)
    print ("####### résultat de découpage en Sous-Séquences : ########")
    print(data)
    
    root = TrieNode('*',[])
    for sequence in data :
     infoNoeud=[]
     infoNoeud.append(sequence[1])
     infoNoeud.append(sequence[2])
     infoNoeud.append(sequence[3])
     add(root, sequence[0],infoNoeud)
    print((time.time()-td)) 

    print("le TRIE CONTIENT ", len(root.children),"BRANCHE")
    variable = input('Quel est LA SÉQUENCE QUE VOUS VOULLEZ CHERCHER ? ')
    type(variable)
    while len((variable))==0:
      variable = input('Quel est LA SÉQUENCE QUE VOUS VOULLEZ CHERCHER ? ')
      type(variable)
    inputs=[]

    for x in variable:
  
      inputs.append(x)
    if len(inputs)>=1 :
      resultat=SeparateWORD(inputs)
    ids=[] 
    for x in resultat:
      id=[]
      _,_,id= find_word(root, x[0])
      ids.append(id)
    
    find=[]
    print(ids)
    if len(resultat)>1:
      Match(ids,find,0,1,"","",len(ids[0]),0)
      xdata=arange(2400)
      fig = plt.figure()
      ax = fig.add_subplot(1, 1, 1)
      ax.plot(xdata, Y, color='tab:blue')
      i=0
      b=False
      m=[]
      for X in find :
        for i in range(0,len(date)):
           
          if str(X[0])==str(date[i]) :
            b=False  
          
            for y in find:
              for j in range(i,len(date)):
                if y[1]==date[j] :
                   n=[]
                   n.append(i)              
                   n.append(j)
                   m.append(n)
                   b=True
                
                if(b):
                  break
              if(b):
                  break   
        


      for x in  m:
        ax.plot(xdata[int(x[0]):int(x[1]+1)], Y[int(x[0]):int(x[1]+1)], color='tab:red')
     ##ax.plot(date,Y[40:100], color='tab:orange')
    
    
    
     
      plt.show()
    elif(len(resultat)==1):
      xdata=arange(2400)
      fig = plt.figure()
      ax = fig.add_subplot(1, 1, 1)
      ax.plot(xdata, Y, color='tab:blue')
      i=0
      b=False
      m=[]
      for X in ids[0] :
        for i in range(0,len(date)):
           
          if str(X[0])==str(date[i]) :
            b=False  
          
            for y in ids[0]:
              for j in range(i,len(date)):
                if y[1]==date[j] :
                   n=[]
                   n.append(i)              
                   n.append(j)
                   m.append(n)
                   b=True
                
                if(b):
                  break
              if(b):
                  break   
                 


      for x in  m:
        ax.plot(xdata[int(x[0]):int(x[1]+1)], Y[int(x[0]):int(x[1]+1)], color='tab:red')
      plt.show()
    
    
    
    
   
    

  
if __name__ == '__main__': 
    main() 
