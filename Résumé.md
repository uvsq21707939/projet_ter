# Résumé

## Titre :

Représentation par un Trie de Séries Temporelles Symboliques.

---

## Auteurs :
- kossi-eloi-fabius Defly <kossi-eloi-fabius.defly@ens.uvsq.fr>
- Billal ZEMMOURA <billal.zemmoura@ens.uvsq.fr>
- Abdelhafid BELHABIB <abdelhafid.belhabib@ens.uvsq.fr>

---

## Introduction et Objectifs :
Une série chronologique est une série de points de données classés dans le temps,  
 elles sont utilisées dans divers domaines tels que: la finance mathématique, la fabrication, les données d'événements (par exemple, les flux de clics et les événements d'application),
les données IoT et généralement dans tout domaine des sciences appliquées et de l'ingénierie qui implique des mesures temporelles.

### Sujets abordés lors de la réunion :   

1. Introduction aux séries temporelles et aux sous séquences temporelles .  
2. les tries et l'intéret de leur utilisation dans notre cas.  

### Objectif :
l'objectif du projet est de construire une structure d’index basée sur le principe des Trie pour pouvoir rechercher efficacement des sous séquences temporelles suivant un motif donné.  

---

## Matériel et méthode :
 
 -langage de programmation:python dans un premier temps puis un essaie sur Scala 
 -extraction des données de courbes des fichiers CSV
 -Représenter ses séquences sous formes de symboles 
 -implémenter une structure Trie qui permet d’indexer 
 -implémenter un opérateur de recherche 
 -mesurer les différentes méthodes de trie sur plusieurs lots de données 

---

## Résultats :

ce projet va nous permmetre de trouver efficacement des sous séquences
temporelles suivant un motif donné
comparer sa performance par rapport à une approche brute (balayage séquentiel)


---



