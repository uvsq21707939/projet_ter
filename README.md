# Projet_TER

## OBJECTIF DU PROJET
L’objectif du projet consiste à construire une structure d’index basée sur le principe des Trie pour
pouvoir rechercher efficacement des sous séquences temporelles suivant un motif donné.
Dans notre cas, le Trie s’applique sur des séquences temporelles discrétisées par la librairie SAX, Une
fois la structure d’index crée, l’étape suivant est d’implémenter un opérateur de recherche des sous
séquences dans l’index basé sur le principe des éxpressions régulières.
Après avoir implémenter une telle structure et l’opérateur associé, un autre objectif est de mesurer
la compacité du Trie sur plusieurs lots de données par rapport à un stockage de base. Puis dans un
second temps, comparer sa performance par rapport à une approche brute (balayage séquentiel).
## 3.1 LA METHODE SAX (Symbolic Aggregate Approximation)
SAX est une représentation symbolique pour les séries chronologiques qui permet la réduction de la
dimensionnalité et l’indexation avec une mesure de distance inférieure. Dans les tâches d’exploration
de données classiques telles que le clustering, la classification supervisée, l’indexation, etc., SAX est
aussi une bonnes représentation par rapport à d’autres aussi bien connues telles que Discrète Wavelet
Transform (DWT) et Transformation de Fourier discrète (DFT), tout en nécessitant moins d’espace de
stockage. En outre, cette représentation permet aux chercheurs de profiter de la richesse des structures
de données et algorithmes en bio-informatique ou text mining, et fournit également des solutions à de
nombreux défis associés aux tâches actuelles d’exploration de données. Un exemple est le découverte
de motif, un problème que nous avons défini pour les données de séries chronologiques.
## 3.1.1 PRINCIPE DE SAX
Dans un objectif de réduction de la dimensionnalité, il est important de définir des unités tempo-
relles permettant de regrouper les points des séries temporelles. On définit généralement des intervalles
du domaine de définition temporel des séries temporelles à représenter. Très généralement, en raison du
coût minime d’acquisition et de stockage des données, les séries temporelles sont enregistrées dans les
bases de données sous la forme la plus détaillée possible, indépendamment de l’échelle de temps à la-
quelle se développent les comportements à identifier. On pourra alors regrouper les points en intervalles
sans perdre d’information essentielle. C’est le principe de la représentation symbolique SAX.
SAX est une représentation symbolique de séries uni-variées centrées réduites qui n’est pas adap-
tative, se calcule en trois étapes :

1. Le domaine temporel est divisé en intervalles de même taille. la série est découpée en segments de longueur L.

2. Calculer la moyenne de la série temporelle sur chaque segment.

3. Quantifier les valeurs moyennes en un symbole choisi dans un alphabet de taille N .
## 3.2 LA METHODE PENTE
l’algorithme de la méthode Pente repose sur trois étapes principales :

1. Diviser la série chronologique en segments de longueur L

2. Calculez la régression linéaire de la série chronologique sur chaque segment.

3. Quantifier ces régressions en un symbole à partir d’un alphabet de taille N.

Par conséquent, pour chaque segment, les régressions linéaires sont calculées puis quantifiées en un
alphabet fini.
La régression linéaire sur chaque segment est calculée en utilisant l’estimation du moindre carré : soit
V1, ..., VL, les valeurs d’une série chronologique V sur le segment temporel T = [t1, ..., tL].
La régression linéaire de V pour T est la fonction linéaire f(x) = sx + b qui minimise la distance entre
l et V pour T.

La régression linéaire de V en T est entièrement décrit par les deux valeurs s et b.
s représente la pente de l
b la valeur prise par l pour x = 0.
l’algorithme de la méthode Pente se base sur la régression linéaire ,dans le but d’avoir une précision
sur les données résultante de l’algorithme
il est  primordiale de choisir les bon paramètres de traitement des données car cela a un impact sur les
résultats de la recherche.
## Auteurs
- kossi-eloi-fabius Defly<kossi-eloi-fabius.defly@ens.uvsq.fr>
- Billal ZEMMOURA <billal.zemmoura@ens.uvsq.fr>
- Abdelhafid BELHABIB <abdelhafid.belhabib@ens.uvsq.fr>
---