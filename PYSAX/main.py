import os
import time
import csv
import math 
import numpy as np
import matplotlib.pyplot as plt
from msax.msax import normalize, paa, sax, breakpoints
from trie import TrieNode,add,shox,find_word
from tslearn.generators import random_walks
from tslearn.preprocessing import TimeSeriesScalerMeanVariance
from tslearn.piecewise import PiecewiseAggregateApproximation
from tslearn.piecewise import SymbolicAggregateApproximation, \
    OneD_SymbolicAggregateApproximation
import numpy as np 
from scipy import *
from scipy import optimize
import time
from scipy import stats
from trie import TrieNode,add,shox,find_word
def SeparateWORD(inputs):
    words=[]
    
    i=0
    while i< len(inputs):
        tmpALPHA=''
        word=[]
        debut=True
        while (ord(inputs[i])>ord(inputs[i+1]) ):
            if  debut:
             tmpALPHA=inputs[i]
             debut=False
            else:
             tmpALPHA=tmpALPHA+inputs[i]
            
            if(i< len(inputs)-2):
                 i+=1
            else:
                 word=[]
                 tmpALPHA=tmpALPHA+inputs[i+1]
                 word.append(tmpALPHA)
                 words.append(word)
                 return words      
        if not debut:
          tmpALPHA=tmpALPHA+inputs[i]
          word.append(tmpALPHA)
          words.append(word)
          word=[]
          if(i< len(inputs)-2): 
                 i+=1
          else:
                 word.append(inputs[i+1])
                 words.append(word)
                 return words
        debut=True
        tmpALPHA=''
        word=[]
        
          
            
        while(ord(inputs[i])<=ord(inputs[i+1])  ):
                if  debut:
                  tmpALPHA=inputs[i]
                  debut=False
                else:
                  tmpALPHA=tmpALPHA+inputs[i]
                if(i< len(inputs)-2): 
                 i+=1
                else:
                 word=[]
                 tmpALPHA=tmpALPHA+inputs[i+1]
                 word.append(tmpALPHA)
                 words.append(word)
                 return words
        if not debut:
                tmpALPHA=tmpALPHA+inputs[i]
                word.append(tmpALPHA)
                words.append(word)
                if(i< len(inputs)-2): 
                 i+=1
                else:
                 word=[]
                 word.append(inputs[i+1])
                 words.append(word)
                 return words
def generateWORD(data):
    words=[]
    
    i=0
    while i< len(data):
        tmpALPHA=''
        tmpDATED=''
        word=[]
        debut=True
        while (ord(data[i][0])>ord(data[i+1][0]) ):
            if  debut:
             tmpALPHA=data[i][0]
             tmpDATED=data[i][1]
             debut=False
            else:
             tmpALPHA=tmpALPHA+data[i][0]
            
            if(i< len(data)-2):
                 i+=1
            else:
                 word.append(tmpALPHA)
                 word.append(tmpDATED)
                 word.append(data[i][2])
                 words.append(word)
                 return words      
        if not debut:
          tmpALPHA=tmpALPHA+data[i][0]
          word.append(tmpALPHA)
          word.append(tmpDATED)
          word.append(data[i][2])
          words.append(word)
          if(i< len(data)-2): 
                 i+=1
        debut=True
        tmpALPHA=''
        tmpDATED=''
        word=[]
        
          
            
        while(ord(data[i][0])<=ord(data[i+1][0])  ):
                if  debut:
                  tmpALPHA=data[i][0]
                  tmpDATED=data[i][1]
                  debut=False
                else:
                  tmpALPHA=tmpALPHA+data[i][0]
                if(i< len(data)-2): 
                 i+=1
                else:
                 word.append(tmpALPHA)
                 word.append(tmpDATED)
                 word.append(data[i][2])
                 words.append(word)
                 return words
        if not debut:
                tmpALPHA=tmpALPHA+data[i][0]
                word.append(tmpALPHA)
                word.append(tmpDATED)
                word.append(data[i][2])
                words.append(word)
                if(i< len(data)-2): 
                 i+=1
def Match(data,resultat,a,b,valD,valF,ymatch,xinit):

  if b<len(data):
    
    for x in range(xinit,ymatch):
      for y in range(0,len(data[b])):
          
            if (data[a][x][2])+1==(data[b][y][2]):
                if (a==0) and (b==len(data)-1):
                 Match(data,resultat,a+1,b+1,data[a][x][0],data[b][y][1],y+1,y)
                elif (a==0) :
                 Match(data,resultat,a+1,b+1,data[a][x][0],valF,y+1,y)
                elif b==len(data)-1:
                 Match(data,resultat,a+1,b+1,valD,data[b][y][1],y+1,y)
                else:
                 Match(data,resultat,a+1,b+1,valD,valF,y+1,y)
            

  else:
    t=[]
    t.append(valD)
    t.append(valF)
    resultat.append(t)
    return resultat

def main():
  resultat=[]
  td=time.time()
  list=[]
  datee=[]
  fname = "test.csv"
  file = open(fname, "rt")

  try:
    reader=csv.reader(file)
    for row in reader:
          tmp=[]
          datee.append(row[0])  
          list.append(float(''.join(row[1])))
          #Y.append(tmp) 
  finally:
    file.close()
  alpha=['a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r']
  z=np.asarray(list)
  w=2
  x_sax = sax(z,w, a=18)
  data=[]

  i=0
  j=0
  for x in x_sax:
       tmp=[]
       tmp.append(alpha[x])
       tmp.append(datee[i])
       tmp.append(datee[i+1])
       data.append(tmp)
       i+=w
  data=generateWORD(data)

  while(j<(len(data))):
   data[j].append(j)
   j+=1 
  print(data) 


  root = TrieNode('*',[0,0])
  for sequence in data :
     date=[]
     date.append(sequence[1])
     date.append(sequence[2])
     date.append(sequence[3])
     add(root, sequence[0],date)
  print((time.time()-td)) 

  print("le TRIE CONTIENT ", len(root.children),"BRANCHE")
  variable = input('Quel est LA SÉQUENCE QUE VOUS VOULLEZ CHERCHER ? ')
  type(variable)
  while len((variable))==0:
   variable = input('Quel est LA SÉQUENCE QUE VOUS VOULLEZ CHERCHER ? ')
   type(variable)
  inputs=[]

  for x in variable:
  
   inputs.append(x)

  if len(inputs)>1 :
   resultat=SeparateWORD(inputs)
  ids=[] 
  for x in resultat:
   id=[]
   _,_,id= find_word(root, x[0])
   ids.append(id)
  print(ids)
  find=[]
  if len(ids)>1:
      Match(ids,find,0,1,"","",len(ids[0]),0)
      xdata=arange(2400)
      fig = plt.figure()
      ax = fig.add_subplot(1, 1, 1)
      ax.plot(xdata, z, color='tab:blue')
      i=0
      b=False
      m=[]
      for X in find :
        print(X[0])
        print(datee[0])
        for i in range(0,len(datee)):
           
          if str(X[0])==str(datee[i]) :
            b=False  
          
            for y in find:
              for j in range(i,len(datee)):
                if y[1]==datee[j] :
                   n=[]
                   n.append(i)              
                   n.append(j)
                   m.append(n)
                   b=True
                
                if(b):
                  break
              if(b):
                  break   
        


      for x in  m:
        ax.plot(xdata[int(x[0]):int(x[1]+1)], z[int(x[0]):int(x[1]+1)], color='tab:red')
     ##ax.plot(date,Y[40:100], color='tab:orange')
      plt.show()

  elif(len(ids)==1):

      print()
      xdata=arange(2400)
      fig = plt.figure()
      ax = fig.add_subplot(1, 1, 1)
      ax.plot(xdata, z, color='tab:blue')
      b=False
      m=[]
      for X in ids[0] :
        for i in range(0,len(datee)):
           
          if str(X[0])==str(datee[i]) :
            b=False  
          
            for y in ids[0]:
              for j in range(i,len(datee)):
                if y[1]==datee[j] :
                   n=[]
                   n.append(i)              
                   n.append(j)
                   m.append(n)
                   b=True
                
                if(b):
                  break
              if(b):
                  break   
                 


      for x in  m:
        ax.plot(xdata[int(x[0]):int(x[1]+1)], z[int(x[0]):int(x[1]+1)], color='tab:red')
      plt.show()

  

if __name__ == "__main__":
  main()
 






